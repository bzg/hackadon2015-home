#+TITLE: Spécifications pour hackadon.org
#+AUTHOR: Bastien Guerry

* Intention

Le site Hackadon a pour but :

1. de faire connaître l'initiative en France et dans le monde

2. de servir d'outil pour les organisateurs de hackadon

* Faire connaître les hackadon

- diffuser le "Hackadon Manifesto" (why?)

- expliquer comment on fait un hackadon (how?)

- présenter une carte et une liste des hackadons (passés et à venir)

- diffuser les vidéos des différents hackadons ([[http://redecentralize.org/interviews/][voir exemple]])

* Outil pour les organisateurs 

- avoir une page par hackadon (passé et à venir)

  - Pour les hackadons archivés :
    - Lieu
    - Date
    - Sponsors
    - Montant collecté auprès des sponsors
    - Nombre de participants
    - Projets présentés
    - Montants redistribués aux projets
    - Liste des vidéos

  - Pour les hackadons à venir :
    - Lieu
    - Date
    - Sponsors
    - Montant collecté auprès des sponsors
    - S'inscrire comme participant
    - Soumettre un projet

- un formulaire d'inscription pour les participants

  - nom
  - prénom
  - email
 
- un formulaire de soumission de projet

  - Nom de votre projet ?
  - Personne(s) pour venir le présenter ?
  - Existe-il une vidéo de présentation ?
  - Site web du projet ?
  - Email de contact de la personne ?
  - Comment votre projet contribue-t-il aux biens communs ?
  - Quel est votre moyen de collecte des dons ?
